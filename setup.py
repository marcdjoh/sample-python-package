from setuptools import find_packages, setup

setup(
    name='mypythonlib',
    version='0.2.0',
    description='My first Python library',
    packages=find_packages(include=['mypythonlib']),
    install_requires=['numpy==1.19.5'],
    setup_requires=['pytest-runner==4.4'],
    tests_require=['pytest==4.4.1'],
    test_suite='tests',
    author='Me',
    license='MIT',
)